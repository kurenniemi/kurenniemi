import os,shutil
from PIL import Image
testdir="./ERKKI"
copydir="./ERKKI-300x300"
i=0
j=0
size = 300, 300
formats=['GIF','JPEG','PNG','TIFF']
extensionspil=['.gif','.jpg','.jpeg','.jpe','.png']
extensionsimagick=['.psd','.tiff','.tif']
for root, dirs, files in os.walk(testdir):
    print root
    copyroot=copydir+root.replace('.','')
    if os.path.isdir(copyroot) == False:
        print copyroot+" must be created"
        os.makedirs(copyroot)
    #print root, "files", files
    for f in files:
        #print f
        fname, ext = os.path.splitext(f)
        #print ext
        if ext.lower() in extensionspil:
            rootfile=root+'/'+f
            copyfile=copyroot+'/'+f
            print rootfile
            print copyfile
            if os.path.exists(copyfile)==False:
                print rootfile +" must be copied and resized"
                shutil.copy2(rootfile,copyfile)
                try:
                    im = Image.open(copyfile)
                    im.thumbnail(size, Image.ANTIALIAS)
                    if ext.lower()=='.gif':
                        savemode='GIF'
                    if ext.lower()=='.jpg':
                        savemode='JPEG'
                    if ext.lower()=='.jpeg':
                        savemode='JPEG'
                    if ext.lower()=='.jpe':
                        savemode='JPEG'
                    if ext.lower()=='.png':
                        savemode='PNG'
                    if ext.lower()=='.tiff':
                        savemode='TIFF'
                    if ext.lower()=='.tif':
                        savemode='TIFF'
                    im.save(copyfile, savemode)
                except:
                    print 'file '+copyfile+' could not be resized from PIL'
            j+=1
        if ext.lower() in extensionsimagick:
            rootfile=root+'/'+f
            copyfile=copyroot+'/'+f
            print rootfile
            print copyfile
            if os.path.exists(copyfile)==False:
                print rootfile +" must be copied and resized"
                shutil.copy2(rootfile,copyfile)
                if ext.lower()=='.psd' or ext.lower()=='.tiff' or ext.lower()=='.tif':
                    os.system('mogrify -resize 300 '+copyfile.replace(' ','\ '))
            j+=1

    i+=1

print 'Number of directories '+str(i)    
print 'Number of images jpg '+str(j)

