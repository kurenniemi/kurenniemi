(function ($) {
    $(document).ready(function () {
             $("#timeout").html("Timeout is "+timeout+' maxed is '+maxed).css("backgroundColor", "silver");

        // RECEIVE IFRAME WINDOW MESSAGES & MAP TO A BODY "END" EVENT
        $(window).bind("message", function (event) {
            // console.log("blog received message", event);
            if (event.originalEvent.origin === "http://kurenniemi.activearchives.org") {
                $("body").trigger("end");
            }
        });
        $("body").bind("end", function () {
            // console.log("body.end triggered!");
	    if(maxed==1){
            $(this).addClass("ended");
            // follow the next link
		var nextp=$("a[rel=next]").first();
		//alert(nextp.length);
		if(nextp){
                  nextp.each(function () {
                            window.location.href = $(this).attr("href");

            });}else{
                            window.location.href = "?p=1";

}
            }});

        $(document).bind("idle.idleTimer", function(){
            $("#msg_timer").html("Please, move the mouse to stay on the page").css("backgroundColor", "silver");
             ctrl=0;
             t=setTimeout(function(){callForNext('idle timer');},tempo);
        });
        
        $(document).bind("active.idleTimer", function(){
             ctrl=1;
             $("#msg_timer").html("User activity detected").css("backgroundColor", "yellow");

        });
        
        $.idleTimer(timeout);
        /*
        $("iframe#frameurl").bind("load", function () {
            $(this).contents().find("body").each(function () {
                console.log(this);
            }).bind("end", function () {
                // iframe has signaled end!
            });
        });
        */

    });
})(jQuery);

function callForNext(str){
     if(ctrl<1){
             $("#msg_timer").html("Next page will be called from "+str).css("backgroundColor", "pink");
            // follow the next link
		var nextp=$("a[rel=next]").first();
		//alert(nextp.length);
		if(nextp.length>0){
                  nextp.each(function () {
                            window.location.href = $(this).attr("href");

            });}else{
                            window.location.href = "?p=1";

}
     }else{
            $("#msg_timer").html("User reset, session continues ").css("backgroundColor", "yellow");

}
}
