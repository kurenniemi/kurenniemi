<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); 

?>
<div id="container2048">

<?php
    $url = get_post_meta($post->ID, "url", true);
    if ($url){ ?>
<div id="containerleft2048">
<iframe  id="frameurl" class="frame2048" src="<?php echo $url; ?>"></iframe>
</div>
<div  id="containerright2048" role="main" >
<div class="wrap2048">
			<?php
			/* Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>
</div>
  </div>
			<br style="clear:both;" />
      <?php }else{?>
<div id="content" role="main" >
			<?php
			/* Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>
  </div>

      <?php }?>

		</div><!-- #container -->
</body>
</html>
<?php 
  //get_sidebar(); 
?>
<?php 
  get_footer(); 
?>
