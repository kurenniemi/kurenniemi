#! /usr/bin/env python
#
# DATA RADIO BOT
# based on same bot by joel rosdahl
#

"""
DATA RADIO BOT!
"""

import irc.bot
# import irc.strings
# from irc.client import ip_numstr_to_quad, ip_quad_to_numstr
import thread, os
from time import sleep
import datetime, sqlite3

# hmm, a bug in bot.py: PATCH ?!
irc.client.VERSION = [str(x) for x in irc.client.VERSION]

###############################################
##############################################
#############################################
from settings import DBNAME, LATENCY

STOP_KEYS = {
    'vendor': True,
    'encoder': True,
    'status': True,
    'rid': True,
    'on_air': True,
    'temporary': True,
    'initial_uri': True,
    'filename': True,
    'replaygain_track_gain': True,
    'replaygain_track_peak': True,
    'source': True
}

AUDIO_LEVELS = {
    (110, 140): "Jet engine at 100 m",
    110: "Non-electric chainsaw at 1 m",
    100: "Jack hammer at 1 m",
    (80, 90): "Traffic on a busy roadway at 10 m",
    (85, ): "Hearing damage (over long-term exposure, need not be continuous)",
    (60, 80): "Passenger car at 10 m",
    65: "Handheld electric mixer",
    60: "TV (set at home level) at 1 m",
    (42,53): "Washing machine, dishwasher",
    (40, 60): "Normal conversation at 1 m",	
    (20, 30): "Very calm room",
    10: "Light leaf rustling, calm breathing",
    0: "Auditory threshold at 1 kHz"
}

metafilters = []
def mf_replaygain(kv):
    (key, value) = kv
    if (key.lower() == "replay_gain"):
        value = value.strip()
        (number, units) = value.split(" ", 1)
        number = float(number)
        target = 89.0
        apparent_level = target - number
        
        text = """According to the <a href="http://kurenniemi.activearchives.org/dataradio/programs.html#replaygain" target="sf">ReplayGain</a> algorithm"""
        if value.startswith("-"):
            value = value[1:]
            text += " the original audio has an average apparent loudness of %s, or relatively loud, and has been reduced by %d to match the baseline loudness of the dataradio." % (apparent_level, value)
        else:
            text += " the original audio has an average apparent loudness of %s, or relatively quiet, and has been amplified by %d to match the baseline loudness of the dataradio." % (apparent_level, value)
        value = text
        return text
metafilters.append(mf_replaygain)

def relay_incoming_messages(bot):
    latency_td = datetime.timedelta(0, LATENCY)
    now = datetime.datetime.utcnow()
    conn = sqlite3.connect(DBNAME)
    c = conn.cursor()
    lastid = None
    for (_id, _type, message, ts) in c.execute("SELECT * FROM erkki_messages ORDER BY id"):
        ts = datetime.datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
        ts += latency_td
        if ts >= now:
            break
        lastid = _id
    # print "Waiting for id's newer than %s" % lastid
    while True:
        try:
            now = datetime.datetime.utcnow()
            if lastid:
                rows = c.execute("SELECT * from erkki_messages WHERE id>? ORDER BY id", (lastid, ))
            else:
                rows = c.execute("SELECT * from erkki_messages ORDER BY id")

            for (_id, _type, message, ts) in rows:
                # print "MESSAGE", _id
                ts = datetime.datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
                ts += latency_td
                # print now, ts
                if now >= ts:
                    didtitle = False
    #                print "*", message
                    lines = message.splitlines()
                    kvpairs = {}
                    for line in message.splitlines():
                        (key, value) = line.strip().split(":", 1)
                        if key.lower() == "title":
                            bot.do_metadata(key, value)
                            didtitle = True
                        else:
                            kvpairs[key] = value
                    if not didtitle:
                        # print "NO TITLE SET", kvpairs
                        # do we have a filename
                        if 'filename' in kvpairs:
                            fname = kvpairs['filename']
                            # print "USING FILENAME", fname
                            _, fname = os.path.split(fname)
                            # print "TITLE", fname
                            bot.do_metadata("title", fname)
                            del kvpairs['filename']
                    keys = kvpairs.keys()
                    keys.sort()
                    for key in keys:
                        bot.do_metadata(key, kvpairs[key])
                    lastid = _id
        except sqlite3.OperationalError:
            pass
        except irc.client.ServerNotConnectedError:
            pass
        sleep(1.0)
    conn.close()
#############################################
##############################################
###############################################

class DataRadioBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.relay_thread_id = None

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)

    def on_privmsg(self, c, e):
        pass
#        print "on_privmsg"
#        self.do_command(e, e.arguments[0])

    def do_metadata(self, key, value):
        if (key == "title"):
            self.connection.topic(self.channel, value.strip())
        else:
            if key not in STOP_KEYS:
                msg = key + ": " + value
                self.connection.privmsg(self.channel, msg)

    def on_pubmsg(self, c, e):
#        print "on_pubmsg", e.source.nick, e.arguments[0]
        nick = e.source.nick
        msg = e.arguments[0]
        conn = sqlite3.connect(DBNAME)
        c = conn.cursor()
        c.execute("INSERT INTO erkki_irc (nick, message, tstamp) VALUES (?, ?, DATETIME('NOW'))", (nick, msg, ))
        conn.commit()
        conn.close()

    def on_dccmsg(self, c, e):
        pass

    def on_dccchat(self, c, e):
        pass

    def on_join(self, c, e):
#        print "JOINED"
        if self.relay_thread_id == None:
            self.relay_thread_id = thread.start_new_thread(relay_incoming_messages, (self,))

def main():
    import sys
    if len(sys.argv) != 4:
        print "Usage: drbot <server[:port]> <channel> <nickname>"
        sys.exit(1)

    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print "Error: Erroneous port."
            sys.exit(1)
    else:
        port = 6667
    channel = sys.argv[2]
    nickname = sys.argv[3]

    bot = DataRadioBot(channel, nickname, server, port)
    bot.start()

def start(channel, nickname, server, port=6667):
    bot = DataRadioBot(channel, nickname, server, port)
    bot.start()


if __name__ == "__main__":
    main()
