#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sqlite3, sys, datetime
from time import sleep
from settings import DBNAME

conn = sqlite3.connect(DBNAME)
c = conn.cursor()
for (_id, _type, message, ts) in c.execute("SELECT * FROM erkki_messages ORDER BY id"):
    ts = datetime.datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
    print "ID:", _id
    print "TYPE:", _type
    print "MESSAGE:", message
    print "TS:", ts
    print "------"


