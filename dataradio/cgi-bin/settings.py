DBNAME = '/var/www/vhosts/kurenniemi.activearchives.org/cgi-bin/dataradio/ircmessages.db'
LATENCY = 10
DAYSTART_HOUR = 6
STOP_KEYS = {
    'vendor': True,
    'encoder': True,
    'status': True,
    'rid': True,
    'on_air': True,
    'temporary': True,
    'initial_uri': True
#    'filename': True
}

