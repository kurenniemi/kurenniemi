"""
Having problems with timing, I compare with the built in scriptreplay command.
The built in works and mine doesn't. What gives!?
I download the source!
util-linux-2.21/
 * Copyright (C) 2008, Karel Zak <kzak@redhat.com>
 * Copyright (C) 2008, James Youngman <jay@gnu.org>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * Based on scriptreplay.pl by Joey Hess <joey@kitenet.net>

HEY... let's look at the PERL script...

http://www.ifokr.org/bri/presentations/tools/scriptreplay
Joey Hess <joey@kitenet.net>

#!/usr/bin/perl -w

# "script -t" will output a typescript with timings
# this script "scriptreplay" replays it
# run pod2man on it to get a man page

=head1 NAME

scriptreplay - play back typescripts, using timing information

=head1 SYNOPSIS

scriptreplay timingfile [typescript [divisor]]

=head1 DESCRIPTION

This program replays a typescript, using timing information to ensure that
output happens at the same speed as it originally appeared when the script
was recorded. It is only guaranteed to work properly if run on the same
terminal the script was recorded on.

The timings information is what script outputs to standard error if it is
run with the -t parameter.

By default, the typescript to display is assumed to be named "typescript",
but other filenames may be specified, as the second parameter.

If the third parameter exits, it is used as a time divisor. For example,
specifying a divisor of 2 makes the script be replayed twice as fast.

=head1 EXAMPLE

 % script -t 2> timingfile
 Script started, file is typescript
 % ls
 <etc, etc>
 % exit
 Script done, file is typescript
 % scriptreplay timingfile

=cut

use strict;
$|=1;
open (TIMING, shift)
        or die "cannot read timing info: $!";
open (TYPESCRIPT, shift || 'typescript')
        or die "cannot read typescript: $!";
my $divisor=shift || 1;

# Read starting timestamp line and ignore.
<TYPESCRIPT>;

my $block;
my $oldblock='';
while (<TIMING>) {
        my ($delay, $blocksize)=split ' ', $_, 2;
        # Sleep, unless the delay is really tiny. Really tiny delays cannot
        # be accurately done, because the system calls in this loop will
        # have more overhead. The 0.0001 is arbitrary, but works fairly well.
        if ($delay / $divisor > 0.0001) {
                select(undef, undef, undef, $delay / $divisor - 0.0001);
        }

        read(TYPESCRIPT, $block, $blocksize)
                or die "read failure on typescript: $!";
        print $oldblock;
        $oldblock=$block;
}
print $oldblock;

=head1 SEE ALSO

script(1)

=head1 COPYRIGHT

This program is in the public domain.

=head1 AUTHOR

Joey Hess <joey@kitenet.net>

"""

import sys, time

name = sys.argv[1]
try:
    divisor = float(sys.argv[2])
except IndexError:
    divisor = 1.0

timing = open(name+".timing")
typescript = open(name+".script")
block = None
oldblock = ''

firstline = typescript.readline()
print firstline

def output (b):
#    sys.stdout.write(b)
#    sys.stdout.flush()
    print "-"*40
    print b


for line in timing:
    (delay, blocksize) = line.split(" ", 1)
    delay = float(delay)
    blocksize = int(blocksize)
    if (delay / divisor > 0.0001):
        time.sleep((delay/divisor)-0.0001)

    block = typescript.read(blocksize)

    output(oldblock)
    # sys.stdout.write(oldblock)
    # sys.stdout.flush()
    oldblock=block

output(oldblock)
#sys.stdout.write(oldblock)
#sys.stdout.flush()

