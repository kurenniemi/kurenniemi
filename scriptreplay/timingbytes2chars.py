#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
This script turns the byte lengths of the native timing format
to character lengths. This is useful when loading in JavaScript, 
as the UTF-8 source data will be converted to UTF-16 in JS leading
to slight differences in character vs. byte lengths in the presence
of non ASCII chars.
"""

import sys

name = sys.argv[1]
timing = open(name+".timing")
typescript = open(name+".script")
block = None
firstline = typescript.readline()
for line in timing:
    (delay, blocksize) = line.split(" ", 1)
    blocksize = int(blocksize)
    block = typescript.read(blocksize)
    block = block.decode("utf-8")
#    if len(block) != blocksize:
#        print len(block), "!=", blocksize
    print delay+" "+str(len(block))



