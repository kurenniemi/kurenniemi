function play (vt, charbuffer, timingdata, elt) {
    var that = {},
        divisor = 1.0,
        timing = timingdata.split("\n"),
        timinglen = timing.length,
        timingindex = 0,
        charpos = 0,
        charlen = charbuffer.length,
        block,
        curtiming,
        paused = false,
        timeout_id;

    function parseTiming (t) {
        var parts = t.split(" "),
            ret = {
                delay: parseFloat(parts[0], 10),
                blocksize: parseInt(parts[1], 10)
            };
        return ret;
    }

    if (!timing[timinglen-1]) {
        // console.log("removing dead timing");
        timinglen--;
        timing.length = timinglen;
    }
    // console.log(timinglen);
    var firstlinelen = charbuffer.indexOf("\n")+1;
    firstline = charbuffer.substr(0, firstlinelen);
    charpos = firstlinelen;
//    console.log("1>", firstline);
//    console.log("2>", charbuffer.substr(charpos, 2));
    vt.clear();
    vt.refresh();
    block = "";
    var displaypat = new RegExp("^#\\s*display\\s*\"?(.*?)\"?$", "m");
    function _t () {
        vt.write(block);
        var m = displaypat.exec(block);
        if (m) {
            // console.log("m", m);
            var display_url = m[1];
            $("#display").text("");
            $("<img />").attr("src", display_url).appendTo("#display");
        }
        // console.log(timingindex, charpos, curtiming);
        // console.log(block);

        if (curtiming !== undefined) {
            block = charbuffer.substr(charpos, curtiming.blocksize);
            charpos += curtiming.blocksize;
        }
        if (timingindex >= timinglen) {
            vt.write(block);
            $("body").trigger("end");
            return;
        }
        curtiming = parseTiming(timing[timingindex++]);
        if (!paused) {
            timeout_id = window.setTimeout(_t, (curtiming.delay/divisor)*1000);
        }
    }
    _t();

    that.setDivisor = function (d) {
        divisor = d;
    }

    that.restart = function () {
        vt.clear();
        vt.refresh();
        block = "";
        timingindex = 0;
        chartiming = undefined;
        charpos = firstlinelen;
        $("#display").text("");
        that.play();
    }

    that.pause = function () {
        if (paused) return;
        paused = true;
        if (timeout_id) {
            window.clearTimeout(timeout_id);
            timeout_id = undefined;
        }
        if (elt) { $(elt).trigger("pause"); }
    }

    that.play = function () {
        if (!paused) return;
        paused = false;
        if (!timeout_id) {
            _t();
        }
        if (elt) { $(elt).trigger("play"); }
    }

    that.toggle = function () {
        paused ? that.play() : that.pause();
    }

    return that;
}


