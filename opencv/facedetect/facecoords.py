#!/usr/bin/python
import cv, sys #Import functions from OpenCV
if len(sys.argv) > 2:
    imname=sys.argv[1]
else:
    imname='00000009.png'

cv.NamedWindow('a_window', cv.CV_WINDOW_AUTOSIZE)
image=cv.LoadImage(imname, cv.CV_LOAD_IMAGE_COLOR) #Load the image
#Load the haar cascade
hc = cv.Load("haarcascade_frontalface_alt2.xml")
#Detect face in image
faces = cv.HaarDetectObjects(image, hc, cv.CreateMemStorage(), 1.2,2, cv.CV_HAAR_DO_CANNY_PRUNING, (10,10) )
if faces:
    for (x,y,w,h),n in faces:
        cv.Rectangle(image, (x,y), (x+w,y+h), 255)
        print imname,' x: ',x,' y: ',y,' width: ',w,' height: ',h
        cv.SetImageROI(image, (x, y, w, h))
        imtmp = cv.CreateImage(cv.GetSize(image),image.depth,image.nChannels)
        imtmpname=sys.argv[2]+'-'+str(x)+'-'+str(y)+'-'+str(w)+'-'+str(h)+'.jpg'
        cv.Copy(image,imtmp)
        cv.SaveImage(imtmpname,imtmp)
        cv.SetImageCOI(image, 0)
#cv.ShowImage('a_window', image) #Show the image
#cv.WaitKey(10000)
