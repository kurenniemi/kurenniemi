#include "cv.h"
#include "highgui.h"

int main( int argc, char** argv )
{
    IplImage* src;
    // the first command line parameter must be file name of binary
    // (black-n-white) image
    if( argc == 2 && (src=cvLoadImage(argv[1], 0))!= 0)
    {
        IplImage* dst = cvCreateImage( cvGetSize(src), 8, 3 );
        CvMemStorage* storage = cvCreateMemStorage(0);
        CvSeq* contour = 0;

        cvThreshold( src, src, 30, 255, CV_THRESH_BINARY );
        cvNamedWindow( "Source", 1 );
        cvShowImage( "Source", src );

        cvFindContours( src, storage, &contour, sizeof(CvContour),
           CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
        cvZero( dst );
        cvRectangle(dst,cvPoint(0,0),cvPoint(src->width,src->height),CV_RGB(255,255,255),CV_FILLED);

        for( ; contour != 0; contour = contour->h_next )
        {
            CvScalar color = CV_RGB( rand()&255, rand()&255, rand()&255 );
            /* replace CV_FILLED with 1 to see the outlines */
//            cvDrawContours dst, contour, color, color, -1, CV_FILLED, 8 );
            cvDrawContours( dst, contour, color,color, -1, 1, 8 );

        }

        cvNamedWindow( "Components", 1 );
        cvShowImage( "Components", dst );
        cvSaveImage("contours.jpg",dst);
        cvWaitKey(0);
    }
}
