#-------------------------------------------------
#
# Project created by QtCreator 2012-05-11T02:42:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = detectcolors
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

CONFIG +=link_pkgconfig
PKGCONFIG += opencv
