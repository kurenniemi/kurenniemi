/*ALL the necessary header files*/
#include"math.h"
#include"cv.h"
#include"highgui.h"
#include"stdio.h"
#include <iostream>
using namespace std;

int main(int argc, char** argv)
{
    if(argc>1){
//cout << "filein: "<<argv[1]<<" fileout: " <<argv[2]<<"\n";
int i,j,k;
int height,width,step,channels;
int stepr, channelsr;
int mindiff=10;
int coldiffb=0;
int coldiffg=0;
int coldiffr=0;
int totalpix=0;
int thechannel=0;
int notchannel1;
int notchannel2;
int theamount=0;
int coeff=0;
char buffer[250];
char filebuffer[510];
char* colornames[] = {"blue", "green", "red"};
CvFont font;
CvSize text_size;
cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 2.0, 2.0, 0, 1, CV_AA);
//int temp=0;
uchar *data,*datar;
i=j=k=0;
//IplImage *frame=cvLoadImage("/home/nicolas/Public/kurenniemi-erkki/from_disk/kuvia/noisecity-pics/IMG_2685.JPG",1);
//IplImage *frame=cvLoadImage("/home/nicolas/code/qt/fog.jpg",1);
IplImage *frame=cvLoadImage(argv[1],1);

IplImage *result=cvCreateImage( cvGetSize(frame), 8, 3 );
if(frame==NULL ) {
puts("unable to load the frame");exit(0);}
//printf("frame loaded");
/*cvNamedWindow("original",CV_WINDOW_AUTOSIZE);
cvNamedWindow("Result",CV_WINDOW_AUTOSIZE);*/

height = frame->height;
width = frame->width;
step =frame->widthStep;
channels = frame->nChannels;
data = (uchar *)frame->imageData;
/*Here I use the Suffix r to differentiate the result data and the frame data
Example :stepr denotes widthStep of the result IplImage and step is for frame IplImage*/

stepr=result->widthStep;
channelsr=result->nChannels;
datar = (uchar *)result->imageData;

//which color ranks higher?
for(i=0;i < (height);i++) for(j=0;j <(width);j++){

if(((data[i*step+j*channels+0]) > (29+data[i*step+j*channels+1])) && ((data[i*step+j*channels+0]) > (29+data[i*step+j*channels+2]))){
  coldiffb++;  //blue
}else if(((data[i*step+j*channels+1]) > (29+data[i*step+j*channels+0])) && ((data[i*step+j*channels+1]) > (29+data[i*step+j*channels+2]))){
  coldiffg++;  //green
}else if(((data[i*step+j*channels+2]) > (29+data[i*step+j*channels+0])) && ((data[i*step+j*channels+2]) > (29+data[i*step+j*channels+1]))){
  coldiffr++;  //red
}
else{

}
totalpix++;
}
if(coldiffb>coldiffg && coldiffb>coldiffr){
    thechannel=0;
    notchannel1=1;
    notchannel2=2;
    theamount=coldiffb;
}else if (coldiffg>coldiffg && coldiffg>coldiffr) {
    thechannel=1;
    notchannel1=0;
    notchannel2=2;
    theamount=coldiffg;
}else{
    thechannel=2;
    notchannel1=0;
    notchannel2=1;
    theamount=coldiffr;
}
coeff=theamount*100/totalpix;

if(coeff>mindiff){
for(i=0;i < (height);i++) for(j=0;j <(width);j++){

/*As I told you previously you need to select pixels which are
more red than any other color
Hence i select a difference of 29(which again depends on the scene).
(you need to select randomly and test*/
    //BLUE
if(((data[i*step+j*channels+thechannel]) > (29+data[i*step+j*channels+notchannel1])) && ((data[i*step+j*channels+thechannel]) > (29+data[i*step+j*channels+notchannel2]))){
datar[i*stepr+j*channelsr+thechannel]=data[i*step+j*channels+thechannel];
datar[i*stepr+j*channelsr+notchannel2]=0;
datar[i*stepr+j*channelsr+notchannel1]=0;
}
else{
datar[i*stepr+j*channelsr+notchannel2]=255;
datar[i*stepr+j*channelsr+notchannel1]=255;
datar[i*stepr+j*channelsr+thechannel]=255;
}}
/*If you want to use cvErode you may...*/
/*Destroying all the Windows*/
/*cvShowImage("original",frame);
cvShowImage("Result",result);*/
sprintf (buffer, "%d percent of pixels with a difference of 29 for channel %d on %d pixels",coeff,thechannel,totalpix);
//cvGetTextSize( buffer, &font, &text_size,130);
//cvRectangle(result, cvPoint(10, 130),cvPoint(1000 ,130),cvScalar(0,0,255));
//cvPutText(result, buffer, cvPoint(10, 130), &font, cvScalar(0, 0, 0, 0));
cout << buffer << "\n";
sprintf(filebuffer,"%s_color_%s_pct_%d.jpg",argv[2],colornames[thechannel],coeff);
cout << filebuffer << "\n";
cvSaveImage(filebuffer,result);
/*
cvWaitKey(0);
cvDestroyWindow("original");
cvDestroyWindow("Result");*/
}else{
    sprintf (buffer, "%d percent of pixels for channel %d on %d pixels, below threshold, image not saved.",coeff,thechannel,totalpix);
cout << buffer << "\n";
}
return 0;
    }
else{
    cout << "Usage ./detectcolors filein fileout\n";
    }

}
