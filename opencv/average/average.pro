#-------------------------------------------------
#
# Project created by QtCreator 2012-05-23T14:06:26
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = average
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
CONFIG +=link_pkgconfig
PKGCONFIG += opencv
