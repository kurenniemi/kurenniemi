/* This is a standalone program. Pass an image name as a first parameter
of the program.  Switch between standard and probabilistic Hough transform
by changing "#if 1" to "#if 0" and back */
#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <math.h>
using namespace std;

int main(int argc, char** argv)
{
    int linesdetected, linesthreshold;
    linesthreshold=3;
    linesdetected=0;
    IplImage* src;
    if( argc >1 && (src=cvLoadImage(argv[1], 0))!= 0)
    {
        IplImage* dst = cvCreateImage( cvGetSize(src), 8, 1 );
        IplImage* color_dst = cvCreateImage( cvGetSize(src), 8, 3 );
        CvMemStorage* storage = cvCreateMemStorage(0);
        CvSeq* lines = 0;
        int i;
        cvCanny( src, dst, 50, 200, 3 );
        cvCvtColor( dst, color_dst, CV_GRAY2BGR );
#if 0
        lines = cvHoughLines2( dst,
                               storage,
                               CV_HOUGH_STANDARD,
                               1,
                               CV_PI/180,
                               100,
                               0,
                               0 );

        for( i = 0; i < MIN(lines->total,100); i++ )
        {
            float* line = (float*)cvGetSeqElem(lines,i);
            float rho = line[0];
            float theta = line[1];
            CvPoint pt1, pt2;
            double a = cos(theta), b = sin(theta);
            double x0 = a*rho, y0 = b*rho;
            pt1.x = cvRound(x0 + 1000*(-b));
            pt1.y = cvRound(y0 + 1000*(a));
            pt2.x = cvRound(x0 - 1000*(-b));
            pt2.y = cvRound(y0 - 1000*(a));
            cvLine( color_dst, pt1, pt2, CV_RGB(255,0,0), 3, 8 );
        }
#else
        lines = cvHoughLines2( dst,
                               storage,
                               CV_HOUGH_PROBABILISTIC,
                               1,
                               CV_PI/180,
                               80,
                               30,
                               10 );
        for( i = 0; i < lines->total; i++ )
        {
            CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i);
            int diffy;
            if(line[0].y>line[1].y){
                diffy=line[0].y-line[1].y;
            }else{
                diffy=line[1].y-line[0].y;
            }
            if(diffy>50){
            cvLine( color_dst, line[0], line[1], CV_RGB(255,255,0), 1, CV_AA);
            linesdetected++;
            //cout << "linesdetected: " << linesdetected <<"\n";
            }else{
            cvLine( color_dst, line[0], line[1], CV_RGB(255,0,255), 1, CV_AA);
            }
        }
#endif
        //cvNamedWindow( "Source", 1 );
        //cvShowImage( "Source", src );

        //cvNamedWindow( "Hough", 1 );
        //cvShowImage( "Hough", color_dst );
        cout << "linesdetected: " << linesdetected <<"\n";

        if(linesdetected<linesthreshold){
        CvPoint cross1,cross2,cross3,cross4;
        cross1.x=0;
        cross1.y=0;
        cross2.x=color_dst->width;
        cross2.y=color_dst->height;
        cross3.x=color_dst->width;
        cross3.y=0;
        cross4.x=0;
        cross4.y=color_dst->height;
//        cout << "cross1 x: " << cross1.x << " cross1 y: " << cross1.y <<"\n";
//        cout << "cross2 x: " << cross2.x << " cross2 y: " << cross2.y <<"\n";
//        cout << "cross3 x: " << cross3.x << " cross3 y: " << cross3.y <<"\n";
//        cout << "cross4 x: " << cross4.x << " cross4 y: " << cross4.y <<"\n";
        cvLine( color_dst, cross1, cross2, CV_RGB(255,255,0), 5, CV_AA);
        cvLine( color_dst, cross3, cross4, CV_RGB(255,255,0), 5, CV_AA);
        }
        cvSaveImage(argv[2],color_dst);
        //cvWaitKey(0);
    }
}
