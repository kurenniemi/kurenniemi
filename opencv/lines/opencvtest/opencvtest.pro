#-------------------------------------------------
#
# Project created by QtCreator 2012-05-02T09:35:48
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = opencvtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

CONFIG +=link_pkgconfig
PKGCONFIG += opencv
