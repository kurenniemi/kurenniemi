<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>
Publishing interface for Kurenniemi s archive
</title>
<style>
body{font-size:0.85em;}
img{max-height:128px;}
input[type="submit"]{font-size:0.95em;}
.nav{margin-bottom:20px;}
.block{width:200px;min-height:200px;float:left;margin:0 10px 10px 0;text-align:center;font-size:0.85em;padding:10px;}
.blockfolder{width:200px;min-height:200px;float:left;margin:0 10px 10px 0;text-align:center;font-size:0.85em;padding:10px;background:#EEE;}

.current{background:#DDD}
.img_display{}
.img_meta{margin-top:10px;}
.listing{margin:20px 0 0 0}
.menu_paginate{clear:both;text-align:center;}
.submit{margin-top:10px;}
</style>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
    $(document).ready(function () {
  /* attach a submit handler to the form */
  $("form").submit(function(event) {

    /* stop form from submitting normally */
    event.preventDefault(); 
        
    /* get some values from elements on the page: */
    var $form = $( this ),
        d= $form.find('input[name="d"]' ).val(),
        f= $form.find('input[name="f"]' ).val(),
        public= $form.find('input[name="public"]' ).val(),
        url= $form.attr( 'action' );
//alert(f);
//alert(d);
    /* Send the data using post and put the results in a div */
    $.get( url, { f:f,d:d,public:public },
      function( data ) {
          var content = data;
          var enclosing='#block_'+$form.attr('id');
          //alert(enclosing);
          //alert($(enclosing).css());
          if(public=='n'){
             $form.find('input[name="public"]' ).val('y');
             $form.find('input[type="submit"]').val('Unpublish');
             $(enclosing).css('backgroundColor','#E8FFBA');

}else{
             $form.find('input[name="public"]' ).val('n');
             $form.find('input[type="submit"]').val('Publish');
             $(enclosing).css('backgroundColor','#FFF0F0');


}
          $('#debug').empty().append(data);
  }
    );
  });
  });

</script>
</head>
<body>

<?php
include('utils.php');
include('class.paginator.php');
//echo __FILE__;
$based=getcwd();
$basepublicd=ereg_replace('private','public',$based);
$ipp_default=50;
if(isset($_GET['ipp'])){
  $ipp=$_GET['ipp'];
}else{
  $ipp=$ipp_default;

}
if(isset($_GET['d'])){
  $d=$_GET['d'];
}else{
  $d='.';

}
if(isset($_GET['order'])){
  $order=$_GET['order'];
}else{
  $order='alpha';

}
if(isset($_GET['filter'])){
  $filter=$_GET['filter'];
}else{
  $filter='all';

}
//toggle_public('.','ls.php');

      $str_order='&order='.$order;
      $str_filter='&filter='.$filter;
$addstr=$str_order.$str_filter;

echo breadcrumb($d,$addstr);

$ls=list_dir($d,$order,$filter);

if($ls){
  $pages = new Paginator;
  $pages->items_total = count($ls);
  $pages->mid_range = 9;
  $pages->default_ipp=$ipp_default;
  $pages->setIpp();
  $pages->paginate($d,$order,$filter);
  echo '<div class="menu_paginate">'.$pages->display_pages().'</div>'."\n";
  echo '<div class="listing">'."\n";
  /* print_r($ls); */
  $end_block=0;
  if(isset($_GET['page']) && is_numeric($_GET['page'])){
    $j=($_GET['page']-1);
    $end_block=$_GET['page'];
  }else{
    $j=0;
    $end_block=1;

  }
  for($i=($j*$ipp);$i<($end_block*$ipp);$i++){
if($i<count($ls)){
    if($ls[$i]['type']=='dir'){
      print "<div class=\"blockfolder\"><a href=\"ls.php?d=".urlencode_parts($d).'/'.urlencode($ls[$i]['name']).$addstr."\">".$ls[$i]['name']."</a> - ".$ls[$i]['type']."</div>\n";
}else{
      if($ls[$i]['public']=='n'){
        $str_style="style=\"background:#FFF0F0\"";
      }else{
        $str_style="style=\"background:#E8FFBA\"";
      }
      print "<div class=\"block\" ".$str_style." id=\"block_".$i."\">\n";
      $end_img='/\.(png|jpg|jpeg|gif)/i';
      $end_tiff='/\.(tiff|tif|psd)/i';
      $rep_jpg='.jpg';

      if(preg_match($end_img,$ls[$i]['name'])){
	echo '<div class="img_display"><img src="thumbsforpreview/'.$d.'/'.$ls[$i]['name'].'" /></div>';
	}
       if(preg_match($end_tiff,$ls[$i]['name'])){
	echo '<div class="img_display"><img src="thumbsforpreview/'.$d.'/'.preg_replace($end_tiff,$rep_jpg,$ls[$i]['name']).'" /></div>';
	}
      print '<div class="img_meta"><a href="'.$d.'/'.$ls[$i]['name'].'" target="_blank">'.$ls[$i]['name']."</a> - ".$ls[$i]['type']."\n";
      print "<form id=\"".$i."\" action=\"mv.php\">\n";
      print "<input type=\"hidden\" name=\"f\" value=\"".$ls[$i]['name']."\">\n";
      print "<input type=\"hidden\" name=\"d\" value=\"".$d."\">\n";
      print "<input type=\"hidden\" name=\"public\" value=\"".$ls[$i]['public']."\">\n";

	if($ls[$i]['public']=='n'){

      print "<div class=\"submit\"><input type=\"submit\" value=\"Publish\"></div>\n";
	}else{
      print "<div class=\"submit\"><input type=\"submit\" value=\"Unpublish\"></div>\n";
	}
      print "</form>\n";
      print "</div></div>\n";
}


}
  }
  echo "</div>\n";
  echo '<div class="menu_paginate">'.$pages->display_pages().'</div>';
}
?>
<div id="debug"></div>
</body>
</html>