#!/usr/bin/env python
#-*- coding:utf-8 -*-

# Take a walk!
import os, sys, Image
import pyexiv2, hashlib, random, colorsys

def md5_for_file(f, block_size=2**20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return md5.digest()

def rgb2hex(r, g, b):
    return '#%02x%02x%02x' % (r, g, b)

def get_color(path):
    img = Image.open(path)
    i2 = img.resize((1, 1), Image.ANTIALIAS)
    r, g, b = i2.getpixel((0, 0))
    h, s, v = colorsys.rgb_to_hsv(r/255.0, g/255.0, b/255.0)
    return r, g, b, h, s, v

# cwd = os.getcwd()
try:
    inpath = sys.argv[1]
except IndexError:
    inpath = "."
inpath = os.path.abspath(inpath)
# print inpath

def mv (m, key):
    val = m.get(key)
    if val:
        return val.value
    return ""

count = 0

def sniff (path):
    global count
    try:
        # img = Image.open(path)
        # info = img._getexif()
        meta = pyexiv2.metadata.ImageMetadata(path)
        meta.read()
        ## INTERESTING
        # print '"%s"' % path, img.format, img.size[0], img.size[1],
        w = meta.dimensions[0]
        h = meta.dimensions[1]
        mime = meta.mime_type
        hasthumb = (len(meta.exif_thumbnail.data) > 0)
        date = meta.get("Exif.Image.DateTime")
        if date:
            try:
                dateiso = date.value.isoformat()
            except AttributeError:
                dateiso = ""
        else:
            dateiso = ""

#        for key in meta.exif_keys:
#            print "\t", key, meta.get(key)
#            print "\t", key, str()

        expo = mv(meta, "Exif.Photo.ExposureTime") # aka Shutter speed
        focal = mv(meta, "Exif.Photo.FocalLength")
        flash = mv(meta, "Exif.Photo.Flash")

        make = mv(meta, "Exif.Image.Make")
        model = mv(meta, "Exif.Image.Model")
        if model:
            make = make + " " + model

        count += 1
        r, g, b = "", "", ""
        hexc = ""
        h, s, v = "", "", ""

        thumbs = meta.previews
        if len(thumbs) > 0 and date:
            thumb = meta.previews[0]
            thumbpathbase = (".thumbs/%06d" % count)
            thumbpath =  thumbpathbase + thumb.extension
            thumb.write_to_file(thumbpathbase)
            r, g, b, h, s, v = get_color(thumbpath)
            hexc = rgb2hex(r, g, b)

        print "\t".join([str(x) for x in (count, path, mime, w, h, hasthumb, dateiso, make, expo, focal, flash, hexc, r, g, b, h, s, v)])


    except IOError:
        #print "IOERROR", path
        pass


try:
    os.mkdir(".thumbs")
except OSError:
    pass

from os.path import join, getsize
for root, dirs, files in os.walk(inpath):
    # print "--", root
    if '.thumbs' in dirs:
      dirs.remove('.thumbs')
    for f in files:
        if f.startswith("."): continue
        apath = os.path.abspath(os.path.join(root, f))
        sniff(apath)


#    print sum(getsize(join(root, name)) for name in files),
#    print "bytes in", len(files), "non-directory files"

