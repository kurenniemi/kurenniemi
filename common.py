import os, hashlib

def githash(path, block_size=2**20):
    """ Should produce the same SHA1 hash git uses for a particular file """
    s = hashlib.sha1()
    filesize = os.path.getsize(path)
    s.update("blob %u\0" % filesize)
    f = open(path, "rb")
    while True:
        data = f.read(block_size)
        if not data:
            break
        s.update(data)
    return s.hexdigest()

