#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys, Image, random, iso8601
inpath = sys.argv[1]

draw = Image.new("RGB", (10000, 2500))
(low, high) = (None, None)

def _val(e):
    return int(e)

    # dt = iso8601.parse_date(e)
    # print dt.month, dt.day
    # return (dt.month-1) + (dt.day/32.0)
    # return dt.month
    # return (dt.hour) + ((dt.minute) / 60.0) + ((dt.second) / 3600.0)

fin = open(inpath)
for line in fin:
    (c, f, mime, w, h, icon, date, make, exposure, focal, flash) = line.split("\t")
    c = int(c)
    val = flash
    if not val.strip():
        continue
    val = _val(val) 
    if (low == None or val<low): low = val
    if (high == None or val>high): high = val

print low, high    
fin.close()

fin = open(inpath)
for line in fin:
    (c, f, mime, w, h, icon, date, make, exposure, focal, flash) = line.split("\t")
    c = int(c)
    val = flash
    if not val.strip():
        continue
    val = _val(val) 
    p = (float(val)-low)/(high-low)
    x = int( p * (draw.size[0]-100) )
    y = random.randint(0, (draw.size[1]-100))
    tpath = ".thumbs/%06d.jpg" % c
    print tpath
    try:
        thumb = Image.open(tpath)
        draw.paste(thumb, (x, y))
    except IOError:
        print "Error reading", tpath

draw.save(".thumbs/index.png")
